import LeftBg from './image/aaron.jpg';
import RightBg from './image/neon.jpg';
import Logo from './image/logo.png';
import Fb from './image/fb.png';
import Heart from './image/heart.png';
import Twitter from './image/twitter_.png';

import Instagram from './image/instagram.png';
import Linkedin from './image/linkedin.png';
import Smiley from './image/smiley.png';

export {
    LeftBg, RightBg,
    Logo, Fb, Heart,Instagram, Linkedin, Smiley,Twitter
}