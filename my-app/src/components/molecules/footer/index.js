import React from 'react'
import { Fb, Heart, Instagram, Linkedin, Logo, Smiley, Twitter } from '../../../assets'
import './footer.scss'

const Icon=({img})=>{
    return (
        <div className="icon-wrapper">
            <img className="icon-medsos" src={img} alt='icon' />
        </div>
    )
}
const Footer = () => {
    return (
        <div>
            <div className="footer">
                <div >
                    <img src={Logo} className='logo' alt='logo' />
                </div>
                <div className="social-wrapper">
                    <Icon img={Fb} alt='fb'  />
                    <Icon src={Twitter} alt='twitter' />
                    <Icon src={Instagram} alt='ig'  />
                    <Icon src={Linkedin} alt='linkedIn' />
                    <Icon src={Smiley} alt='smile' />
                    <Icon src={Linkedin} alt='linkedIn'/>
                </div>

                <p>Akun media Sosial</p>
            </div>
            <div className='copyright'>
                <p>Copyright</p>
            </div>
        </div>
    )
}

export default Footer
// 3:09
// https://www.youtube.com/watch?v=sbVE0CFyPxY&list=PLU4DS8KR-LJ0-MT2QfV-fvJiNorsoFs74&index=10