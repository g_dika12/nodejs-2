import React from 'react'
import { useHistory } from 'react-router-dom';
import { RightBg } from '../../../assets'
import { Button, Gap } from '../../atoms';
import './blogItem.scss';

const BlogItem = () => {
    const history = useHistory();
    return (
        <div className='blog-item'>
            <img className='image-thumb' src={RightBg} alt='post' />
            <div className='content-detail'>
                <p className="title">Title Blog</p>
                <p className="author">Author - Date post</p>
                <p className="body">Disclaimer Copyright Under Article 107 of the Copyright Act 1976, allowances are made for "fair use" for purposes such as criticism, comments, news reporting, teaching, scholarships, and research. Fair use is the use permitted by copyright laws that may be violated. Nonprofit, education, or personal use, balance tips for fair use. This video is for entertainment only. No copyright violations are intended. </p>
                <Gap height={20} />
                <Button title="view detail" onClick={()=>history.push('/detail-blog')} />
            </div>
        </div>
    )
}

export default BlogItem
