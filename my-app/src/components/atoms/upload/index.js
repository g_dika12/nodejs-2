import React from 'react'
import { LeftBg } from '../../../assets'
import './upload.scss'

const UploadImage = () => {
    return (
        <div className="upload">
            <img src={LeftBg} className='preview' alt='preview' />
            <input type="file" />
        </div>
    )
}

export default UploadImage
