import React from 'react'
import { useHistory } from 'react-router-dom';
import { RightBg } from '../../assets';
import { Input, Button, Gap, Link } from '../../components';

function Login() {
    const history = useHistory()
    return (
        <div className='main-page'>
            <div className='left'>
                <img src={RightBg} className="bg-image" alt='imageBg' />
            </div>
            <div className='right'>
                <p className='title'>Login</p>
                
                <Input label="Email" placeholder="email" />
                <Gap height={18} />
                <Input label="Password" placeholder="password" />
                <Gap height={50} />
                <Button title='Login' onClick={()=> history.push('/')} />
                <Gap height={100} />
                <Link title="Belum punya akun? silahkan daftar" onClick={()=> history.push('/register')} />
            </div>
        </div>
    )
}

export default Login
