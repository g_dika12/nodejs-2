import React from 'react'
import { useHistory } from 'react-router-dom';
import { RightBg } from '../../assets'
import { Gap, Link } from '../../components';
import './detailBlog.scss';

const DetailBlog = () => {
    const history = useHistory()
    return (
        <div className="detail-blog-wrapper">
            <img className="img-cover" src={RightBg} alt='thumb' />
            <p className="blog-title">Title Blog</p>
            <p className="blog-author">Author - Date Post</p>
            <p className="blog-body">Bridge Dev and Ops once and for all. Our industry-leading CI/CD empowers all teams to work together efficiently. Powerful, scalable, end-to-end automation is possible with GitLab..Manage projects, not tools. With GitLab, you get an open DevOps platform delivered as a single application—one interface, one conversation thread, one data store, zero headaches.</p>
            <Gap height={20} />
            <Link title='Kembali ke Home' onClick={()=>history.push('/')} />
        </div>
    )
}

export default DetailBlog
