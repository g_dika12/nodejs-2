import React from 'react'
import { useHistory } from 'react-router-dom';
import { LeftBg } from '../../assets';
import { Input, Button, Gap, Link } from '../../components';
import './register.scss';

function Register() {
    const history = useHistory()
    return (
        <div className='main-page'>
            <div className='left'>
                <img src={LeftBg} className="bg-image" alt='imageBg' />
            </div>
            <div className='right'>
                <p className='title'>Form Register</p>
                <Input label="Full Name" placeholder="full name" />
                <Gap height={18} />
                <Input label="Email" placeholder="email" />
                <Gap height={18} />
                <Input label="Password" placeholder="password" />
                <Gap height={50} />
                <Button title='Register' onClick={()=>history.push('/login')} />
                <Gap height={100} />
                <Link title="Kembali ke login" onClick={()=>history.push('/login')} />
            </div>
        </div>
    )
}

export default Register
