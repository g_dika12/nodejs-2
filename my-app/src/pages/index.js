import Home from './home'
import Login from './login'
import Register from './register'
import CreateBlog from './createBlog'
import DetailBlog from './detailBlog'

export {
    Home,
    Login,
    Register,
    CreateBlog,
    DetailBlog
}