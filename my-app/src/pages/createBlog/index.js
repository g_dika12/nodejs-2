import React from 'react'
import { Input, Button,Gap, Upload, TextArea, Link } from "../../components";
import './createBlog.scss'
import { useHistory } from "react-router-dom";
export default function CreateBlog() {
    const history = useHistory()
    return (
        <div className="blog-post">
            <Link title="kembali" onClick={()=>history.push('/')} />
            <p className='title'>Create New Blog Post</p>
            <Input label="Post Title" />
            <p>Upload Image</p>
            <Upload />
            <TextArea />
            <Gap height={20} />
            <div className="button-action">
            <Button title="Save" />
            </div>
        </div>
    )
}
